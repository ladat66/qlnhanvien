<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\nhanvien;

class NhanvienController extends Controller
{
    //
    public function getdanhsach(){
    	$nhanvien=nhanvien::all();
    	return view('admin.nhanvien.danhsach',['nhanvien'=>$nhanvien]);
    }
    public function getthem(){
    	return view('admin.nhanvien.them');
    }

    public function postthem(Request $request){
        // echo $request->Ten;
        $this->validate($request,
            [
                'Ten'=> 'required',
                'Ngayky'=>'required',
                'Sdt'=>'required',
                'Diachi'=>'required'
            ],
            [
                'Ten.required'=>'Bạn chưa nhập tên',
                'Ngayky.required'=>'Bạn chưa nhập ngày đăng ký',
                'Sdt.required'=>'Bạn chưa nhập sdt',
                'Diachi.required'=>'Bạn chưa nhập địa chỉ'
                
            ]
        );

        $nhanvien = new nhanvien;
        $nhanvien->tennv=$request->Ten;
        $nhanvien->ngayky=$request->Ngayky;
        $nhanvien->sdt=$request->Sdt;
        $nhanvien->diachi=$request->Diachi;
        
         $nhanvien->save();
        // echo changeTitle($request->Ten);
         return redirect('admin/nhanvien/them')->with('thongbao','Thêm thành công');

    }

    public function getsua ($id){
        $nhanvien = nhanvien::find($id);
        return view('admin.nhanvien.sua',['nhanvien'=>$nhanvien]);
    }

    public function postsua(Request $request,$id ){
        $nhanvien = nhanvien::find($id);
        $this->validate($request,
            [
                'Ten'=> 'required',
                'Ngayky'=>'required',
                'Sdt'=>'required',
                'Diachi'=>'required'
            ],
            [
                'Ten.required'=>'Bạn chưa nhập tên',
                'Ngayky.required'=>'Bạn chưa nhập ngày đăng ký',
                'Sdt.required'=>'Bạn chưa nhập sdt',
                'Diachi.required'=>'Bạn chưa nhập địa chỉ'
                
            ]
        );

       
        $nhanvien->tennv=$request->Ten;
        $nhanvien->ngayky=$request->Ngayky;
        $nhanvien->sdt=$request->Sdt;
        $nhanvien->diachi=$request->Diachi;
        
         $nhanvien->save();
         return redirect('admin/nhanvien/sua/'.$id)->with('thongbao','Sửa thành công');
    }
    public function getxoa($id){
        $nhanvien=nhanvien::find($id);
        $nhanvien->delete();
        return redirect('admin/nhanvien/danhsach')->with('thongbao','xóa thành công');
    }

}
