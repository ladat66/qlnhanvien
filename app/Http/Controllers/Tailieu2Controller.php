<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\tailieu;

use Validator;

class Tailieu2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(tailieu::get(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'author' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 201);
        } 

        $tl = tailieu::create([
            'title' => $request->title,
            'author' => $request->author
        ]);

        // $tailieu->title = $request->title;
        // $tailieu->author = $request->author;
        // $result = $tailieu->save();

        return response()->json($tl, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (tailieu::find($id) == null) {

            return response()->json('Document is not found', 404);

        } else {

            return response()->json(tailieu::find($id), 200);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $doc = tailieu::find($id);

        if (is_null($doc)) {

            return response()->json(["message" => 'Document is not found!'], 404);

        }

        $doc->title=$request->title;
        $doc->author=$request->author;
        $doc->save();

        return response()->json(tailieu::find($id), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $doc = tailieu::find($id);

        if (is_null($doc)) {

            return response()->json(["message" => 'Document is not found!'], 404);

        }

        $doc->delete();
        
        return response()->json(null, 204);
    }
}
