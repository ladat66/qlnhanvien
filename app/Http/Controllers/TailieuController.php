<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\tailieu;

use Validator;

class TailieuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tailieu=tailieu::all();

        return view('admin.tailieu.danhsach',['tailieu'=>$tailieu]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tailieu.them');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,
            [
                'title'=> 'required'
            ],
            [
                'title.required'=>'Bạn chưa nhập tên'
                
            ]
        );

        $user = Auth::user();

        $tailieu = new tailieu;
        $tailieu->title = $request->title;
        $tailieu->author = Auth::id();
        
        $tailieu->save();
        // echo changeTitle($request->Ten);
         return redirect('admin/tailieu/create')->with('thongbao','Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function test2()
    {
        $newArr = array();
        foreach (tailieu::get() as $value) {
            $newArr[] = $value->title;
        }
        return response()->json($newArr, 200);
    }

    public function test()
    {
        return response()->json(tailieu::get(), 200);
    }

    public function getById($id)
    {

        if (tailieu::find($id) == null) {

            return response()->json('Document is not found', 404);

        } else {

            return response()->json(tailieu::find($id), 200);

        }

    }

    public function addDoc(Request $request)
    {
        $rules = [
            'title' => 'required',
            'author' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 201);
        } 

        $tl = tailieu::create([
            'title' => $request->title,
            'author' => $request->author
        ]);

        // $tailieu->title = $request->title;
        // $tailieu->author = $request->author;
        // $result = $tailieu->save();

        return response()->json($tl, 201);

        // var_dump ($tl);
        // die();
    }

    public function updatedoc(Request $request, $id)
    {

        $doc = tailieu::find($id);

        if (is_null($doc)) {

            return response()->json(["message" => 'Document is not found!'], 404);

        }

        $doc->title=$request->title;
        $doc->author=$request->author;
        $doc->save();

        return response()->json(tailieu::find($id), 200);

    }

    public function updatedoc2(Request $request, tailieu $doc)
    {

        $doc->update($request->all());
        return response()->json($doc, 200);
    }

     public function deletedoc(tailieu $doc)
    {

        $doc->delete();
        return response()->json(null, 204);
    }

}
