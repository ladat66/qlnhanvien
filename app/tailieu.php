<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tailieu extends Model
{
    //
    protected $table = 'tailieu';
    // public $timestamps = false;

    protected $fillable = [
        'title', 'author', 'created_at', 'updated_at'
    ];

    public function user(){
    	return $this->belongsTo('App\User','author','id');
    }
}
