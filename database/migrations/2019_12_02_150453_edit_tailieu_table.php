<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTailieuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tailieu', function (Blueprint $table) {
            // $table->collation('utf8_unicode_ci')->change();
            $table->string('author', 100)->collation('utf8_unicode_ci');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tailieu', function (Blueprint $table) {
            // $table->collation('utf8mb4_unicode_ci')->change();
            $table->string('author', 100)->collation('utf8mb4_unicode_ci');
        });
    }
}
