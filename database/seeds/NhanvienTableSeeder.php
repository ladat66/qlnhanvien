<?php

use Illuminate\Database\Seeder;

class NhanvienTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('nhanvien')->insert([
            'tennv' => str_random(10),
            
            'sdt' => '123456789',
            'diachi' => '11 Tran Duy Hung',
        ]);
    }
}
