-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2018 at 12:24 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qlnhanvien`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2018_10_08_081011_create_nhanvien_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `id` int(10) UNSIGNED NOT NULL,
  `tennv` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ngayky` datetime NOT NULL,
  `sdt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diachi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`id`, `tennv`, `ngayky`, `sdt`, `diachi`, `created_at`, `updated_at`) VALUES
(1, 'Nguyen Van C', '2018-10-11 00:00:00', '01234574', 'Hue', NULL, NULL),
(2, 'Nguyen van VV', '2018-12-12 00:00:00', '34665647', 'Hai trieu', '2018-10-13 01:52:30', '2018-10-13 01:52:30'),
(5, 'Nguyen van Vu 2', '2018-12-12 00:00:00', '34665647', 'Hai trieu', '2018-10-13 02:23:11', '2018-10-17 01:23:14'),
(6, 'dat la la', '2017-07-04 00:00:00', '08675643', 'Ha noi', '2018-10-17 01:24:02', '2018-10-17 01:24:02');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'La Huu Dat', 'ladat66@gmail.com', NULL, '$2y$10$CEdbdsSMU9Nv.6yjdRMEtOhR0kdIiOBWtNR2Bup9upjueOPbcsM9m', 'CErj8qqFNczMVxcCJvDnsYrsaL5B4BRrdALJ4OoWyDeN5xkH8K6LqIerCSQ8', NULL, NULL),
(2, 'user22', '123456@gmail.com', NULL, '$2y$10$zH7wiMh/oknA5DMpHaEyPOYxvZ4T6Pwbirb8JpPp0.MyjYsZyrx.y', NULL, '2018-10-15 21:10:34', '2018-10-16 02:29:40'),
(3, 'Dat', 'huudat@gmail.com', NULL, '$2y$10$NH2XVLxagyoiv8xs1F5Ar.Yc.IKPRe6jbE2PhwJoejwdC352trzBu', NULL, '2018-10-16 02:39:49', '2018-10-16 02:39:49'),
(4, 'Đạt La Hữu', 'ladat6fndfndn6@gmail.com', NULL, '$2y$10$.k49Vq99Uc56.oXcnaE1tOP.u9PnqyhkPoNJCFETdLSqAS5m4uKVm', NULL, '2018-10-16 02:41:36', '2018-10-16 02:41:36'),
(5, 'fnd gn', 'lagsdg@gfjfg.com', NULL, '$2y$10$LEXupPkXjpu5Itimd5dBwuKxQ3EMmAPDWNlLpmwGFU9J3pkbYpJX.', NULL, '2018-10-16 02:43:59', '2018-10-16 02:43:59'),
(7, 'dat hh', 'huudat222@gmail.com', NULL, '$2y$10$ATxUfi6n6y6u4PjNCwv2guQVWYQpCJuJ7tt5i6bELQH1oOBB/S9Qi', 'TE2izyV678rmhiUeCTaZ5fZTirDPn7YDSO9lvquhE1obVIZlGmbikDFZVv94', '2018-10-16 02:49:49', '2018-10-16 02:49:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nhanvien`
--
ALTER TABLE `nhanvien`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
