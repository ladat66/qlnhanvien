@extends('admin.layout.index')

@section('content')
    {{-- expr --}}

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Nhân viên
                    <small>{{ $nhanvien->tennv  }}</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
            @if(count($errors)>0)
            <div class="alert alert-dangrer">
                @foreach ($errors->all() as $err)
                    {{$err}}<br>
                @endforeach
            </div>
            @endif

            @if (session('thongbao'))
                <div class="alert alert-success">
                    {{ session('thongbao') }}
                </div>
            @endif

                <form action="admin/nhanvien/sua/{{$nhanvien->id }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Tên thể loại</label>
                        <input class="form-control" name="Ten" placeholder="Điền tên thể loại" value="{{ $nhanvien->tennv }}" />
                    </div>


                    <div class="form-group">
                        <label>Ngày đăng ký </label>
                        <div id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd">
                            <input value="{{ str_replace(" 00:00:00", "", $nhanvien->ngayky)  }}" class="form-control" type="text" name="Ngayky" id="myDate" readonly />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Số điện thoại</label>
                        <input class="form-control" name="Sdt"  placeholder="Điền số điện thoại" value="{{ $nhanvien->sdt }}" />
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ</label>
                        <input class="form-control" name="Diachi" placeholder="Điền địa chỉ" value="{{ $nhanvien->diachi }}" />
                    </div>
                    
                    <button type="submit" class="btn btn-default">Sửa</button>
                    <button type="reset" class="btn btn-default">Làm mới</button>
                    <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@endsection

@section('script')
    <script>
        var x = document.getElementById("myDate").value;
        $(function () {
          $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
        }).datepicker('update', x);
      });
    </script>
@endsection