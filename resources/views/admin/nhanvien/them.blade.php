@extends('admin.layout.index')

@section('content')
    {{-- expr --}}

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Nhân viên
                    <small>Thêm</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if (count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $err)
                            {{ $err }}<br>
                        @endforeach
                    </div>
                @endif


                @if (session('thongbao'))
                    <div class="alert alert-success">
                        {{ session('thongbao') }}
                    </div>
                @endif

                <form action="admin/nhanvien/them" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Tên nhân viên</label>
                        <input class="form-control" name="Ten" placeholder="Nhập tên nhân viên" />
                    </div>
                    {{-- <div class="form-group">
                        <label>Ngày đăng ký</label>
                        <input class="form-control" name="Ngayky" placeholder="Nhập ngày đăng ký" />
                    </div> --}}

                    <div class="form-group">
                        <label>Ngày đăng ký </label>
                        <div id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd">
                            <input class="form-control" type="text" name="Ngayky" readonly />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Số điện thoại</label>
                        <input class="form-control" name="Sdt" placeholder="Nhập số điện thoại" />
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ</label>
                        <input class="form-control" name="Diachi" placeholder="Nhập địa chỉ" />
                    </div>
                    
                    

                    <button type="submit" class="btn btn-default">Thêm</button>
                    <button type="reset" class="btn btn-default">Làm mới</button>
                <form>
            </div>
        </div>
                <!-- /.row -->
    </div>
            <!-- /.container-fluid -->
</div>

@endsection

@section('script')
    <script>
        $(function () {
          $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
        }).datepicker('update', new Date());
      });
    </script>
@endsection