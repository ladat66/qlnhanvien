@extends('admin.layout.index')

@section('content')
    {{-- expr --}}

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tài liệu
                    <small>danh sách</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            @if (session('thongbao'))
            <div class="alert alert-success">
                {{ session('thongbao') }}
            </div>
            @endif


            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Tên tài liệu</th>
                       <th>Tác giả</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tailieu as $tl)
                        <tr class="odd gradeX" align="center">
                            <td>{{ $tl->id }}</td>
                        <td>{{ $tl->title }}</td>
                        <td>{{ $tl->user->name }}</td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
        <!-- /#page-wrapper -->

@endsection