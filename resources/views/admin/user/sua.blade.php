@extends('admin.layout.index')

@section('content')
    {{-- expr --}}

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User
                    <small>
                        {{ $user->name }}
                    </small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">

                @if (count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $err)
                            {{ $err }}<br>
                        @endforeach
                    </div>
                @endif


                @if (session('thongbao'))
                    <div class="alert alert-success">
                        {{ session('thongbao') }}
                    </div>
                @endif

                <form action="admin/user/sua/{{ $user->id }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Họ tên</label>
                        <input class="form-control" name="name" placeholder="nhập tên người dùng" value="{{ $user->name }}" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" readonly="" placeholder="Nhập địa chỉ email" value="{{ $user->email }}" />
                    </div>
                    <div class="form-group">
                        <input type="checkbox" id="changePassword1" name="changePassword">
                        <label>Đổi mật khẩu</label>
                        <input type="password" class="form-control password" name="password" placeholder="Nhập mật khẩu" disabled="" />
                    </div>
                    <div class="form-group">
                        <label>Nhập lại password</label>
                        <input type="password" name="passwordAgain" class="form-control password" placeholder="Nhập lại mật khẩu" disabled="" />
                    </div>

                    
                    <button type="submit" class="btn btn-default">Sửa</button>
                    <button type="reset" class="btn btn-default">Làm mới</button>
                <form>
            </div>
        </div>
                <!-- /.row -->
    </div>
            <!-- /.container-fluid -->
</div>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#changePassword1").change(function(){
                if($(this).is(":checked")){
                    $(".password").removeAttr('disabled');
                }else{
                    $(".password").attr('disabled','');
                }
            });
        });
    </script>
@endsection