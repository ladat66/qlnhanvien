<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::get('testapi','TailieuController@test');

// Route::get('getbyid/{id}','TailieuController@getById');

// Route::post('adddoc','TailieuController@addDoc');

// Route::put('updatedoc/{id}','TailieuController@updatedoc');

// Route::put('updatedoc2/{doc}','TailieuController@updatedoc2');

// Route::delete('deletedoc/{doc}','TailieuController@deletedoc');

Route::apiResource('tailieu2','Tailieu2Controller');