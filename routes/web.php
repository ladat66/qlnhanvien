<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return redirect('admin/dangnhap');
});


Route::get('admin/dangnhap','UserController@getDangnhapAdmin');
Route::post('admin/dangnhap','UserController@postDangnhapAdmin');
Route::get('admin/logout','UserController@getDangxuatAdmin');

Route::get('admin/dangky','UserController@getthem');
Route::post('admin/them','UserController@postthem');

Route::group(['prefix'=>'admin','middleware'=>'adminLogin'],function(){
	Route::group(['prefix'=>'nhanvien'],function(){
		Route::get('danhsach','NhanVienController@getdanhsach');
		Route::get('sua/{id}','NhanVienController@getsua');
		Route::post('sua/{id}','NhanVienController@postsua');

		Route::get('them','NhanVienController@getthem');
		Route::post('them','NhanVienController@postthem');

		Route::get('xoa/{id}','NhanVienController@getxoa');
	});

	Route::group(['prefix'=>'user'],function(){
		Route::get('danhsach','UserController@getdanhsach');
		Route::get('sua/{id}','UserController@getsua');
		Route::post('sua/{id}','UserController@postsua');
		// Route::get('them','UserController@getthem');

		// Route::post('them','UserController@postthem');
		Route::get('xoa/{id}','UserController@getxoa');
	});

	Route::group(['prefix'=>'tailieu'],function(){
		Route::get('create','TailieuController@create');
		Route::post('store','TailieuController@store');
		Route::get('index','TailieuController@index');
		
	});

});

